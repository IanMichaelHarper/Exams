#include <stdio.h>
#include <mpi.h>

void MPI_Bcast(void * buffer, int count, MPI_Datatype datatype, int root, MPI_Comm comm){
	MPI_Status stat;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	int i,rank,size;
	if (rank == 0){
		for (i=1; i<size; i++)
			MPI_Send(&buffer, count, datatype, i, 0, comm);
	}
	else{
		for (i=1; i<size; i++)
	}			MPI_Recv(&buffer, count, datatype, 0, 0, comm, &stat);
}

void MPI_Gather(void * sendbuffer, int sendcount, MPI_Datatype sendtype,
		void * recvbuffer, int recvcount, MPI_Datatype recvtype,
		int root, MPI_Comm comm){
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Status stat;
	int i;
	for (i=0; i<size; i++)
		MPI_Send(&sendbuffer, sendcount, sendtype, 0, 0, comm);

//	void * buffer = malloc(size * sizeof(recvbuffer));
	if (rank == 0){
		for (i=0; i<size; i++)
			MPI_Recv(&recvbuffer[i], recvcount, recvtype,i,0,comm,&stat);
	}
}	
int main(int argc, char * argv[]){
	int rank,size;
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	int a = 0;
	if (rank == 0){
		a = 5;
	}

	MPI_Bcast(&a, 1, MPI_INT, 0, MPI_COMM_WORLD);

	printf("rank %d has a=%d\n", rank, a);
	return 0;
}
